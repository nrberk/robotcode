import serial
from time import sleep
from math import *
import sys

#This code is imported from create.py, it sets up some constants for the code
START = chr(128)    # already converted to bytes...
BAUD = chr(129)     # + 1 byte
CONTROL = chr(130)  # deprecated for Create
SAFE = chr(131)
FULL = chr(132)
POWER = chr(133)
SPOT = chr(134)     # Same for the Roomba and Create
CLEAN = chr(135)    # Clean button - Roomba
COVER = chr(135)    # Cover demo - Create
MAX = chr(136)      # Roomba
DEMO = chr(136)     # Create
DRIVE = chr(137)    # + 4 bytes
MOTORS = chr(138)   # + 1 byte
LEDS = chr(139)     # + 3 bytes
SONG = chr(140)     # + 2N+2 bytes, where N is the number of notes
PLAY = chr(141)     # + 1 byte
SENSORS = chr(142)  # + 1 byte
FORCESEEKINGDOCK = chr(143)  # same on Roomba and Create
# the above command is called "Cover and Dock" on the Create
DRIVEDIRECT = chr(145)       # Create only
STREAM = chr(148)       # Create only
QUERYLIST = chr(149)       # Create only
PAUSERESUME = chr(150)       # Create only

tty = serial.Serial (port="/dev/tty.Geometry-ElementSerial", baudrate=57600, timeout=0.01)
out= True #This will be used to exit the loop
print(bytes(1))
print(bytes(2))
print(bytes(3))

def easyFunction(serialObject, list):
        i = 0
        while(i < len(list)):
                serialObject.write(chr(int(list[i])))
                i = i + 1

def playSong(serialObject): #plays a defined song
        test = [128, 132, 140, 0, 8, 62, 12, 66, 12, 69, 12, 74, 36, 74, 15, 69, 12, 66, 12, 62, 20, 141, 0]
        easyFunction(serialObject, test)
        
def stop(serialObject):#stops the robot
        test = [137, 0, 0, 0, 0]
        easyFunction(serialObject, test)

def backwards(serialObject): #moves the robot backwards
        test = [137, 255, 56, 0, 0]
        easyFunction(serialObject, test)

def forwards(serialObject):#moves the robot forwards
        test = [137, 1, 44, 128, 0]
        easyFunction(serialObject,test)

def forwardStop(serialObject):#should move the robot in a square, doesn't yet.
        test = [152, 24, 128, 132, 140, 0, 8, 62, 12, 66, 12, 69, 12, 74, 36, 74, 15, 69, 12, 66, 12, 62, 20, 141, 0, 153, 153]
        easyFunction(serialObject, test)
        test = [152, 6, 128, 137, 1, 44, 128, 0, 153]
        easyFunction(serialObject, test)
        sleep(2)
        test = [152, 6, 128, 137, 0, 0, 0, 0, 153]
        easyFunction(serialObject, test)

def spinLeft(serialObject): #spins the robot to the left
        test = [137, 1, 44, 0, 1]
        easyFunction(serialObject, test)

def spinRight(serialObject): #spins the robot to the right
        test = [137, 1, 44, 255, 255]
        easyFunction(serialObject, test)

def readBumpSensor(serialObject): #reads the robot bump sensor, and will display when it has been hit. It will stop running after it has been hit.
        sense = True
        time = 0
        while (sense == True):
                test = [142, 7]
                easyFunction(serialObject, test)
                testByte = serialObject.read()
                testBit = testByte & bytes(1)
                print(testBit)
                time = time + 1
                if (time == 100): 
                        sense = False
        print("Done reading the sensor")

def bitOfByte( bit, byte ): #from create
    """ returns a 0 or 1: the value of the 'bit' of 'byte' """
    if bit < 0 or bit > 7:
        return 0
    return ((byte >> bit) & 0x01)
        

while (out == True):
        print ("\nPlease Select an option Number\n1. Play Song\n2. Stop\n3. Exit\n4. Backwards\n5. Drive and Stop\n6. Forwards\n7. Spin Left\n8. Spin Right\n9. Read Sensor")
        option = int(input("Selection? "))
        if (option == 1):
                playSong(tty)
        elif (option == 2):
                stop(tty)
        elif (option == 3):
                out = False
        elif(option == 4):
                backwards(tty)
        elif(option == 5):
                forwardStop(tty)
        elif(option == 6):
                forwards(tty)
        elif(option == 7):
                spinLeft(tty)
        elif(option == 8):
                spinRight(tty)
        elif(option == 9):
                readBumpSensor(tty)
                
        else:
                print("Command does not exist")
                

