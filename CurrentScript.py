import serial
from time import sleep
from math import *
import sys

#This code is imported from create.py, it sets up some constants for the code
START = chr(128)    # already converted to bytes...
BAUD = chr(129)     # + 1 byte
CONTROL = chr(130)  # deprecated for Create
SAFE = chr(131)
FULL = chr(132)
POWER = chr(133)
SPOT = chr(134)     # Same for the Roomba and Create
CLEAN = chr(135)    # Clean button - Roomba
COVER = chr(135)    # Cover demo - Create
MAX = chr(136)      # Roomba
DEMO = chr(136)     # Create
DRIVE = chr(137)    # + 4 bytes
MOTORS = chr(138)   # + 1 byte
LEDS = chr(139)     # + 3 bytes
SONG = chr(140)     # + 2N+2 bytes, where N is the number of notes
PLAY = chr(141)     # + 1 byte
SENSORS = chr(142)  # + 1 byte
FORCESEEKINGDOCK = chr(143)  # same on Roomba and Create
# the above command is called "Cover and Dock" on the Create
DRIVEDIRECT = chr(145)       # Create only
STREAM = chr(148)       # Create only
QUERYLIST = chr(149)       # Create only
PAUSERESUME = chr(150)       # Create only

tty = serial.Serial (port ="/dev/tty.ElementSerial-ElementSe", baudrate=57600, timeout=0.01)
out= True #This will be used to exit the loop
test = [128, 132]
print("started")
print(bytes(1))
print(bytes(2))
print(bytes(3))

def easyFunction(serialObject, list):
        i = 0
        while(i < len(list)):
                serialObject.write(chr(int(list[i])))
                i = i + 1

def playSong(serialObject): #plays a defined song
        test = [128, 132, 140, 0, 8, 62, 12, 66, 12, 69, 12, 74, 36, 74, 15, 69, 12, 66, 12, 62, 20, 141, 0]
        easyFunction(serialObject, test)

def negSong (serialObject):
	test = [140, 1, 2, 49, 32, 42, 64, 141, 1]
	easyFunction(serialObject, test)

def qSong (serialObject):
	test = [140, 2, 2, 69, 50, 73, 18, 2]
	easyFunction (serialObject, test) 
        
def stop(serialObject):#stops the robot
        test = [137, 0, 0, 0, 0]
        easyFunction(serialObject, test)

def forwardFoot (serialObject): #drives for about a foot
	test = [137, 1, 44, 128, 0, 155, 10]	
	easyFunction(serialObject, test)
	stop(tty)

def backFoot (serialObject): #drives for about a foot
	test = [137, 255, 56, 128, 0, 155, 10]
 	easyFunction(serialObject, test)
	stop(tty)

def nod (serialObject):
	test = [137, 1, 44, 128, 0, 155, 4]	
	easyFunction(serialObject, test)
	test = [137, 255, 56, 128, 0, 155, 4]
	easyFunction(serialObject, test)
	test = [137, 1, 44, 128, 0, 155, 4]	
	easyFunction(serialObject, test)
	test = [137, 255, 56, 128, 0, 155, 4]
	easyFunction(serialObject, test)
	stop(tty);

def backwards(serialObject): #moves the robot backwards
        test = [137, 255, 56, 128, 0]
        easyFunction(serialObject, test)

def shake (serialObject):
	test = [137, 1, 44, 255, 255, 155,4]
	easyFunction(serialObject, test)
	test = [137, 1, 44, 0, 1, 155, 4]
        easyFunction(serialObject, test)
	test = [137, 1, 44, 255, 255, 155,4]
	easyFunction(serialObject, test)
	test = [137, 1, 44, 0, 1, 155, 4]
        easyFunction(serialObject, test)
	stop(tty)

def forwards(serialObject):#moves the robot forwards
        test = [137, 1, 44, 128, 0]
        easyFunction(serialObject,test)

def square(serialObject):#should move the robot in a square, doesn't yet.
        i=0
	test = [137, 144, 128, 0, 156, 11, 44 , 137, 144, 01, 157, 0, 90]
	easyFunction(serialObject, test)
	if (i<4):
		square(tty)
		i=i+1

def spinLeft(serialObject): #spins the robot to the left
        test = [137, 1, 44, 0, 1]
        easyFunction(serialObject, test)

def spinRight(serialObject): #spins the robot to the right
        test = [137, 1, 44, 255, 255]
        easyFunction(serialObject, test)

def readBumpSensor(serialObject): #reads the robot bump sensor, and will display when it has been hit. It will stop running after it has been hit.
        sense = True
        time = 0
        while (sense == True):
                test = [142, 7]
                easyFunction(serialObject, test)
                testByte = serialObject.read()
                testBit = testByte #& bytes(1)
                print(testBit)
                time = time + 1
                if (time == 100): 
                        sense = False
        print("Done reading the sensor")

def bitOfByte( bit, byte ): #from create
    """ returns a 0 or 1: the value of the 'bit' of 'byte' """
    if bit < 0 or bit > 7:
        return 0
    return ((byte >> bit) & 0x01)
        

while (out == True):
        print ("\nPlease Select an option Number\ns- Play Song\nf- Forward for a foot\nw-Fowards\nx- Backwards\na- Spin Left\nd- Spin Right\nEnter- stop\nspace- quit")
        option = raw_input("Selection? ")
	#numOp = ord(option);
	#print (numOp)

	if (option is ''):
		stop(tty)

       	elif(option is 'z'):
                backwards(tty)
	elif(option is 'w'):
                forwards(tty)
	elif (option is 'v'):
		backFoot(tty)
	elif (option is 'f'):
               forwardFoot(tty)	
	
	elif(option is 'a'):
                spinLeft(tty)
        elif(option == 'd'):
                spinRight(tty)
	elif (option is 'g'):
		square(tty)
	
	elif(option is 'b'):
                shake(tty)
        elif(option is 't'):
		nod(tty)
	elif (option is 's'):
                playSong(tty)
	elif (option is 'y'):
		negSong(tty)
	elif (option is 'u'):
		qSong (tty)

	elif(option == 9):
                readBumpSensor(tty)
	elif (option == 0):
                out = False  
	elif (option is ' '):
		sys.exit()     
        else:
                print("Command does not exist")
                

